LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY function_41_tb IS

END function_41_tb;

ARCHITECTURE test OF function_41_tb IS

component function_41
    port (
      S: IN std_logic_vector(2 DOWNTO 0); 
      Y  : OUT std_logic_vector(2 DOWNTO 0)
    );
  end component;

  signal S, Y : std_logic_vector (2 DOWNTO 0);

begin 

  multiplex: function_41 port map(S => S, Y => Y);

  process begin

    S <= "000";
    wait for 10 ns;

    assert Y = "000"
      report "Test S: 000 failed";

  S <= "101";
    wait for 10 ns;

    assert Y = "111"
      report "Test S: 101 failed";

  S <= "110";
    wait for 10 ns;

    assert Y = "111"
      report "Test S: 110 failed";

  S <= "111";
    wait for 10 ns;

    assert Y = "111"
      report "Test S: 111 failed";

    wait;
  end process;
end test;