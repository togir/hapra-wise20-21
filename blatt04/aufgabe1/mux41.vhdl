LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
--the 4:1 multiplexer to be designed consists of four 3-bit inputs, i.e., i1, i2, i3 and i4.
--sel is a 2-bit select signal.
--y is the output
ENTITY mux41 IS
    PORT (
        I0 : IN std_logic_vector(2 DOWNTO 0); 
        I1 : IN std_logic_vector(2 DOWNTO 0);
        I2 : IN std_logic_vector(2 DOWNTO 0);
        I3 : IN std_logic_vector(2 DOWNTO 0);
        sel: IN std_logic_vector(1 DOWNTO 0); 
        Y  : OUT std_logic_vector(2 DOWNTO 0)
        );
END mux41;

ARCHITECTURE rtl OF mux41 IS
--you can add more components you need
--you can also increase the number of inputs, e.g., the "and" gate blow is extended to a 3-input gate.

    COMPONENT andgate IS
        PORT (
          input1 : IN std_logic;
          input2 : IN std_logic;
          and_result : OUT std_logic
          );
    END COMPONENT;

    COMPONENT orgate IS
        PORT (
          input1 : IN std_logic;
          input2 : IN std_logic;
          result : OUT std_logic
          );
    END COMPONENT;

    COMPONENT notgate IS
        PORT (
          input1 : IN std_logic;
          result : OUT std_logic
          );
    END COMPONENT;


--you can define more signals here if you need, e.g.,:
--  signal not_sel : std_logic_vector(1 DOWNTO 0);

    signal S0, S1, not_S0, not_S1 : std_logic;

    -- to check which signal source should be selected
    signal select1, select2, select3, select4: std_logic;

    signal O_00, O_01, O_02 : std_logic;
    signal O_10, O_11, O_12 : std_logic;
    signal O_20, O_21, O_22 : std_logic;
    signal O_30, O_31, O_32 : std_logic;
    signal tmp_010, tmp_230, tmp_011, tmp_231, tmp_012, tmp_232 : std_logic;


BEGIN

S0 <= sel(0);
S1 <= sel(1);
not_i_dont_care1 : notgate port map (input1 => S0, result => not_S0);
not_i_dont_care2 : notgate port map (input1 => S1, result => not_S1);



and_i_dont_care1 : andgate port map ( input1 => not_S0, input2 => not_S1, and_result => select1 );
and_i_dont_care2 : andgate port map ( input1 => S0,     input2 => not_S1, and_result => select2 );
and_i_dont_care3 : andgate port map ( input1 => not_S0, input2 => S1,     and_result => select3 );
and_i_dont_care4 : andgate port map ( input1 => S0,     input2 => S1,     and_result => select4 );

and_i_dont_care5 : andgate port map ( input1 => select1, input2 => I0(0), and_result => O_00 );
and_i_dont_care6 : andgate port map ( input1 => select1, input2 => I0(1), and_result => O_01 );
and_i_dont_care7 : andgate port map ( input1 => select1, input2 => I0(2), and_result => O_02 );

and_i_dont_care8 : andgate port map ( input1 => select2, input2 => I1(0), and_result => O_10 );
and_i_dont_care9 : andgate port map ( input1 => select2, input2 => I1(1), and_result => O_11 );
and_i_dont_care10: andgate port map ( input1 => select2, input2 => I1(2), and_result => O_12 );

and_i_dont_care11: andgate port map ( input1 => select3, input2 => I2(0), and_result => O_20 );
and_i_dont_care12: andgate port map ( input1 => select3, input2 => I2(1), and_result => O_21 );
and_i_dont_care13: andgate port map ( input1 => select3, input2 => I2(2), and_result => O_22 );

and_i_dont_care14: andgate port map ( input1 => select4, input2 => I3(0), and_result => O_30 );
and_i_dont_care15: andgate port map ( input1 => select4, input2 => I3(1), and_result => O_31 );
and_i_dont_care16: andgate port map ( input1 => select4, input2 => I3(2), and_result => O_32 );



or_i_dont_care1 : orgate   port map ( input1 => O_00,    input2 => O_10,    result => tmp_010 );
or_i_dont_care2 : orgate   port map ( input1 => O_20,    input2 => O_30,    result => tmp_230 );
or_i_dont_care3 : orgate   port map ( input1 => tmp_010, input2 => tmp_230, result => Y(0)    );

or_i_dont_care4 : orgate   port map ( input1 => O_01,    input2 => O_11,    result => tmp_011 );
or_i_dont_care5 : orgate   port map ( input1 => O_21,    input2 => O_31,    result => tmp_231 );
or_i_dont_care6 : orgate   port map ( input1 => tmp_011, input2 => tmp_231, result => Y(1)    );

or_i_dont_care7 : orgate   port map ( input1 => O_02,    input2 => O_12,    result => tmp_012 );
or_i_dont_care8 : orgate   port map ( input1 => O_22,    input2 => O_32,    result => tmp_232 );
or_i_dont_care9 : orgate   port map ( input1 => tmp_012, input2 => tmp_232, result => Y(2)    );

END rtl;