LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY mux81 IS
  PORT (
    I0 : IN std_logic_vector(2 DOWNTO 0); 
    I1 : IN std_logic_vector(2 DOWNTO 0);
    I2 : IN std_logic_vector(2 DOWNTO 0);
    I3 : IN std_logic_vector(2 DOWNTO 0);
    I4 : IN std_logic_vector(2 DOWNTO 0);
    I5 : IN std_logic_vector(2 DOWNTO 0);
    I6 : IN std_logic_vector(2 DOWNTO 0);
    I7 : IN std_logic_vector(2 DOWNTO 0);
    S : IN std_logic_vector(2 DOWNTO 0); 
    Y : OUT std_logic_vector(2 DOWNTO 0));
END mux81;

ARCHITECTURE rtl OF mux81 IS

  COMPONENT mux41 IS
      PORT (
        I0 : IN std_logic_vector(2 DOWNTO 0); 
        I1 : IN std_logic_vector(2 DOWNTO 0);
        I2 : IN std_logic_vector(2 DOWNTO 0);
        I3 : IN std_logic_vector(2 DOWNTO 0);
        sel : IN std_logic_vector(1 DOWNTO 0); 
        Y : OUT std_logic_vector(2 DOWNTO 0)
      );
  END COMPONENT;

  COMPONENT notgate IS
      PORT (
        input1 : IN std_logic;
        result : OUT std_logic
        );
  END COMPONENT;

  COMPONENT andgate IS
      PORT (
        input1 : IN std_logic;
        input2 : IN std_logic;
        and_result : OUT std_logic
        );
  END COMPONENT;

  COMPONENT orgate IS
      PORT (
        input1 : IN std_logic;
        input2 : IN std_logic;
        result : OUT std_logic
        );
  END COMPONENT;

  signal m0, m1, and_0, and_1 : std_logic_vector(2 DOWNTO 0);
  signal sel : std_logic_vector(1 DOWNTO 0);
  signal not_S2 : std_logic;
begin

  -- how can I do this properly? Feels like cheating on the vhdl language
  sel(0) <= S(0);
  sel(1) <= S(1);

  not_i_dont_care0: notgate port map (input1 => S(2), result => not_S2);

  -- multiplexer0
  mux_i_dont_care_0 : mux41 port map (I0 => I0, I1 => I1, I2 => I2, I3 => I3, sel => sel, Y => m0);

  -- multiplexer1
  mux_i_dont_care_1 : mux41 port map (I0 => I4, I1 => I5, I2 => I6, I3 => I7, sel => sel, Y => m1);

-- decide which multiplexer to use
and_i_dont_care0: andgate port map (input1 => m0(0), input2 => not_S2,   and_result => and_0(0));
and_i_dont_care1: andgate port map (input1 => m0(1), input2 => not_S2,   and_result => and_0(1));
and_i_dont_care2: andgate port map (input1 => m0(2), input2 => not_S2,   and_result => and_0(2));

and_i_dont_care3: andgate port map (input1 => m1(0), input2 => S(2),     and_result => and_1(0));
and_i_dont_care4: andgate port map (input1 => m1(1), input2 => S(2),     and_result => and_1(1));
and_i_dont_care5: andgate port map (input1 => m1(2), input2 => S(2),     and_result => and_1(2));


-- combine the andoutput
or_i_dont_care0: orgate port map( input1 => and_0(0), input2 => and_1(0), result => Y(0) );
or_i_dont_care1: orgate port map( input1 => and_0(1), input2 => and_1(1), result => Y(1) );
or_i_dont_care2: orgate port map( input1 => and_0(2), input2 => and_1(2), result => Y(2) );


end rtl;