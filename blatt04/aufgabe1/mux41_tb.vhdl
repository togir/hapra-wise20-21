library ieee;
use ieee.std_logic_1164.all;

entity mux41_tb is
end mux41_tb;

architecture testbench of mux41_tb is
  component mux41
    port (
        I0 : IN std_logic_vector(2 DOWNTO 0); 
        I1 : IN std_logic_vector(2 DOWNTO 0);
        I2 : IN std_logic_vector(2 DOWNTO 0);
        I3 : IN std_logic_vector(2 DOWNTO 0);
        sel: IN std_logic_vector(1 DOWNTO 0); 
        Y  : OUT std_logic_vector(2 DOWNTO 0)
    );
  end component;

  signal I0, I1, I2, I3, Y : std_logic_vector(2 DOWNTO 0);
  signal sel : std_logic_vector(1 DOWNTO 0);

begin
    multiplex: mux41 port map(I0 => I0, I1 => I1, I2 => I2, I3 => I3, sel => sel, Y => Y);

    process 
    

    type input_type is array (natural range <>) of std_logic_vector(2 DOWNTO 0);
    constant input_patterns : input_type :=
    (
      "000",
      "001",
      "010",
      "011",
      "100",
      "101",
      "110",
      "111"
    );

    begin
        sel <= "00";
        for i in input_patterns'range loop
          --  Set the inputs.
          I0 <= input_patterns(i);
          I1 <= "000";
          I2 <= "000";
          I3 <= "000";
          wait for 10 ns;
          assert Y = input_patterns(i)
            report "Test sel: 00 failed";
        end loop;

        sel <= "01";
        for i in input_patterns'range loop
          --  Set the inputs.
          I0 <= "000";
          I1 <= input_patterns(i);
          I2 <= "000";
          I3 <= "000";
          wait for 10 ns;
          assert Y = input_patterns(i)
            report "Test sel 01 failed";
          end loop;

          sel <= "10";
          for i in input_patterns'range loop
            --  Set the inputs.
            I0 <= "000";
            I1 <= "000";
            I2 <= input_patterns(i);
            I3 <= "000";
            wait for 10 ns;
            assert Y = input_patterns(i)
              report "Test sel 10 failed";
            end loop;

        
          sel <= "11";
          for i in input_patterns'range loop
            --  Set the inputs.
            I0 <= "000";
            I1 <= "000";
            I2 <= "000";
            I3 <= input_patterns(i);
            wait for 10 ns;
            assert Y = input_patterns(i)
              report "Test sel 11 failed";
            end loop;     
		
        wait;
    end process;
end testbench;