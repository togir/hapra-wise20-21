library ieee;
use ieee.std_logic_1164.all;

entity mux81_tb is
end mux81_tb;

architecture testbench of mux81_tb is
  component mux81
    port (
        I0 : IN std_logic_vector(2 DOWNTO 0); 
        I1 : IN std_logic_vector(2 DOWNTO 0);
        I2 : IN std_logic_vector(2 DOWNTO 0);
        I3 : IN std_logic_vector(2 DOWNTO 0);
        I4 : IN std_logic_vector(2 DOWNTO 0);
        I5 : IN std_logic_vector(2 DOWNTO 0);
        I6 : IN std_logic_vector(2 DOWNTO 0);
        I7 : IN std_logic_vector(2 DOWNTO 0);
        S: IN std_logic_vector(2 DOWNTO 0); 
        Y  : OUT std_logic_vector(2 DOWNTO 0)
    );
  end component;

  signal I0, I1, I2, I3, I4, I5, I6, I7, Y : std_logic_vector(2 DOWNTO 0);
  signal sel : std_logic_vector(2 DOWNTO 0);

begin
    multiplex: mux81 port map(I0 => I0, I1 => I1, I2 => I2, I3 => I3, I4 => I4, I5 => I5, I6 => I6, I7 => I7, S => sel, Y => Y);

    process 
    

    type input_type is array (natural range <>) of std_logic_vector(2 DOWNTO 0);
    constant input_patterns : input_type :=
    (
      "000",
      "001",
      "010",
      "011",
      "100",
      "101",
      "110",
      "111"
    );

    begin

        sel <= "000";
        for i in input_patterns'range loop
          --  Set the inputs.
          I0 <= input_patterns(i);
          I1 <= "000";
          I2 <= "000";
          I3 <= "000";
          I4 <= "000";
          I5 <= "000";
          I6 <= "000";
          I7 <= "000";
          wait for 10 ns;
          assert Y = input_patterns(i)
            report "Test sel: 000 failed";
        end loop;


       sel <= "001";
        for i in input_patterns'range loop
          --  Set the inputs.
          I0 <= "000";
          I1 <= input_patterns(i);
          I2 <= "000";
          I3 <= "000";
          I4 <= "000";
          I5 <= "000";
          I6 <= "000";
          I7 <= "000";
          wait for 10 ns;
          assert Y = input_patterns(i)
            report "Test sel: 001 failed";
        end loop;


        sel <= "010";
        for i in input_patterns'range loop
          --  Set the inputs.
          I0 <= "000";
          I1 <= "000";
          I2 <= input_patterns(i);
          I3 <= "000";
          I4 <= "000";
          I5 <= "000";
          I6 <= "000";
          I7 <= "000";
          wait for 10 ns;
          assert Y = input_patterns(i)
            report "Test sel: 010 failed";
        end loop;


        sel <= "011";
        for i in input_patterns'range loop
          --  Set the inputs.
          I0 <= "000";
          I1 <= "000";
          I2 <= "000";
          I3 <= input_patterns(i);
          I4 <= "000";
          I5 <= "000";
          I6 <= "000";
          I7 <= "000";
          wait for 10 ns;
          assert Y = input_patterns(i)
            report "Test sel: 011 failed";
        end loop;


        sel <= "100";
        for i in input_patterns'range loop
          --  Set the inputs.
          I0 <= "000";
          I1 <= "000";
          I2 <= "000";
          I3 <= "000";
          I4 <= input_patterns(i);
          I5 <= "000";
          I6 <= "000";
          I7 <= "000";
          wait for 10 ns;
          assert Y = input_patterns(i)
            report "Test sel: 100 failed";
        end loop;


        sel <= "101";
        for i in input_patterns'range loop
          --  Set the inputs.
          I0 <= "000";
          I1 <= "000";
          I2 <= "000";
          I3 <= "000";
          I4 <= "000";
          I5 <= input_patterns(i);
          I6 <= "000";
          I7 <= "000";
          wait for 10 ns;
          assert Y = input_patterns(i)
            report "Test sel: 101 failed";
        end loop;


        sel <= "110";
        for i in input_patterns'range loop
          --  Set the inputs.
          I0 <= "000";
          I1 <= "000";
          I2 <= "000";
          I3 <= "000";
          I4 <= "000";
          I5 <= "000";
          I6 <= input_patterns(i);
          I7 <= "000";
          wait for 10 ns;
          assert Y = input_patterns(i)
            report "Test sel: 110 failed";
        end loop;


        sel <= "111";
        for i in input_patterns'range loop
          --  Set the inputs.
          I0 <= "000";
          I1 <= "000";
          I2 <= "000";
          I3 <= "000";
          I4 <= "000";
          I5 <= "000";
          I6 <= "000";
          I7 <= input_patterns(i);
          wait for 10 ns;
          assert Y = input_patterns(i)
            report "Test sel: 111 failed";
        end loop;   
		
        wait;
    end process;
end testbench;