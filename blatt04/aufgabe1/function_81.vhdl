LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY function_81 IS
    PORT (
      S: IN std_logic_vector (2 DOWNTO 0);
      Y: OUT std_logic_vector(2 DOWNTO 0)
    );
END function_81;

ARCHITECTURE rtl OF function_81 IS

  component mux81
    port (
      I0 : IN std_logic_vector(2 DOWNTO 0); 
      I1 : IN std_logic_vector(2 DOWNTO 0);
      I2 : IN std_logic_vector(2 DOWNTO 0);
      I3 : IN std_logic_vector(2 DOWNTO 0);
      I4 : IN std_logic_vector(2 DOWNTO 0);
      I5 : IN std_logic_vector(2 DOWNTO 0);
      I6 : IN std_logic_vector(2 DOWNTO 0);
      I7 : IN std_logic_vector(2 DOWNTO 0);
      S  : IN std_logic_vector(2 DOWNTO 0); 
      Y  : OUT std_logic_vector(2 DOWNTO 0)
    );
  end component;


begin
mux_i_dont_care_0 : mux81 port map (
  I0 => "000",
  I1 => "111",
  I2 => "111",
  I3 => "111",
  I4 => "000",
  I5 => "111",
  I6 => "111",
  I7 => "111",
  S => S,
  Y => Y
);
end rtl;