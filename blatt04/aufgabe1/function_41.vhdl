LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY function_41 IS
    PORT (
      S: IN std_logic_vector (2 DOWNTO 0);
      Y: OUT std_logic_vector (2 DOWNTO 0)
    );
END function_41;

ARCHITECTURE rtl OF function_41 IS

component mux41
    port (
      I0 : IN std_logic_vector(2 DOWNTO 0); 
      I1 : IN std_logic_vector(2 DOWNTO 0);
      I2 : IN std_logic_vector(2 DOWNTO 0);
      I3 : IN std_logic_vector(2 DOWNTO 0);
      sel: IN std_logic_vector(1 DOWNTO 0); 
      Y  : OUT std_logic_vector(2 DOWNTO 0)
    );
  end component;

  signal S_only_two : std_logic_vector (1 DOWNTO 0);

begin

  S_only_two(0) <= S(0);
  S_only_two(1) <= s(1);

  mux_i_dont_care_0 : mux41 port map (
    I0 => "000",
    I1 => "111",
    I2 => "111",
    I3 => "111",
    sel => S_only_two,
    Y => Y
  );
end rtl;