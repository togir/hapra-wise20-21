library ieee;
use ieee.std_logic_1164.all;

entity sl2_tb is
end entity sl2_tb;

architecture test of sl2_tb is
component sl2 is
        port (
                a: in std_logic_vector(31 downto 0);
                y: out std_logic_vector(31 downto 0)        
        );
        
end component;
        signal a,y : std_logic_vector(31 downto 0) := x"00000000";

begin

        test_thing: sl2 port map (
               a,
               y 
        );

        process
        begin
                a <= "11000000000000000000000000000000";
                wait for 10 ns;
                assert y = "00000000000000000000000000000000"
                        report "Wert in y ist falsch"
                        severity error;
                
                wait;
        end process;

end architecture;