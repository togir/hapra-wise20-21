library ieee;
use ieee.std_logic_1164.all;

entity bit_recognition_input is

  port (
    CLK, --"CLK" is the fsm clock
    reset, --"reset" controls when the machine is reset to original state
    value_in  : in std_logic;--value_in is the fsm input,
    bitmuster : in std_logic_vector(3 downto 0); -- the input
    output    : out std_logic); --output is the fsm output

end bit_recognition_input;


  -- Bei der modifizierung unseres programmes ist leider der automat wegoptimiert worden. Er wird nun durch ein Schieberegister simmuliert. Aus externer sicht verhält es sich aber äquivalent zu einem Automaten, kann also genauso genutzt werden.


architecture moore_bit_rgn_input of bit_recognition_input is

  signal schieberegister : std_logic_vector(3 downto 0);
  signal already_accepted : std_logic := '0';
begin

  process (CLK)
  begin

    if(CLK = '1' and CLK'event) then
      schieberegister(0) <= schieberegister(1);
      schieberegister(1) <= schieberegister(2);
      schieberegister(2) <= schieberegister(3);

      if(reset = '1') then
        schieberegister(3) <= 'U';
        already_accepted <= '0';
      else
        schieberegister(3) <= value_in;
      end if;

      report "Schieberegister has value: " & 
      std_logic'image(schieberegister(0))
      & std_logic'image(schieberegister(1))
      & std_logic'image(schieberegister(2))
      & std_logic'image(schieberegister(3));
    end if;

    if (already_accepted = '1') then
      output <= '1';
    else

  -- all bits of schieberegister must match the input
  -- schieberegister is not allwoed to contain undefined
      if (
        schieberegister(0) /= 'U' and
        schieberegister(1) /= 'U' and
        schieberegister(2) /= 'U' and
        schieberegister(3) /= 'U' and
        schieberegister(0) = bitmuster(0) and
        schieberegister(1) = bitmuster(1) and
        schieberegister(2) = bitmuster(2) and
        schieberegister(3) = bitmuster(3)
      ) then
        output <= '1';
        already_accepted <= '1';
      else
        output <= '0';
      end if;
    
    end if;

  end process;

end moore_bit_rgn_input;