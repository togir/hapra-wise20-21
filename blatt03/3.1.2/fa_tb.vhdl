library ieee;
use ieee.std_logic_1164.all;

entity fa_tb is
end fa_tb;

architecture test of fa_tb is
  component fa
    port (
      a : in std_logic;
      b : in std_logic;
      cin : in std_logic;
      cout : out std_logic;
      sum : out std_logic
    );
  end component;

signal a, b, cin, cout, sum : std_ulogic;
begin
  full_adder: fa port map(a => a, b => b, cin => cin, cout => cout, sum => sum);

  process 
    type pattern_type is record
        --  The inputs of the gates.
        a, b, cin, cout, sum : std_logic;
    end record;
    type pattern_array is array (natural range <>) of pattern_type;

    constant test_array : pattern_array :=
    (
      ('0', '0', '0', '0', '0'),
      ('0', '0', '1', '0', '1' ),
      ('0', '1', '0', '0', '1' ),
      ('0', '1', '1', '1', '0' ),
      ('1', '0', '0', '0', '1' ),
      ('1', '0', '1', '1', '0' ),
      ('1', '1', '0', '1', '0' ),
      ('1', '1', '1', '1', '1' )
    );
  
  begin
    
    -- I've added the test cases as array so we can now loop over them

    for i in test_array'range loop
      
      --  Set the inputs.
      a   <= test_array(i).a;
      b   <= test_array(i).b;
      cin <= test_array(i).cin;
      
      --  Wait for the inputs to be set
      wait for 1 ns;

      --  Check the outputs.
      assert sum = test_array(i).sum  
        report std_logic'image(a) & ", " & std_logic'image(b) & ", " & std_logic'image(cin) & " reported sum: " & std_logic'image(sum) & " but should be " & std_logic'image(test_array(i).sum);

    assert cout = test_array(i).cout
      report std_logic'image(a) & ", " & std_logic'image(b) & ", " & std_logic'image(cin) & " reported carry_out: " & std_logic'image(cout) & " but should be " & std_logic'image(test_array(i).cout);

    end loop;


    wait;
  end process;
end test;
