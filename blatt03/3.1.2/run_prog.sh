#/bin/bash

set -e
set -u
set -x
set -eo pipefail

VHDL_FILE="$1"

ghdl -s "$VHDL_FILE.vhdl" || { echo "Syntax check failed"; exit 1; }

ghdl -a "$VHDL_FILE.vhdl" || { echo "Analyse failed"; exit 1; }

ghdl -e "$VHDL_FILE" || { echo "Elaboration failed"; exit 1; }

ghdl -r "$VHDL_FILE" --vcd="$VHDL_FILE.vcd" || { echo "Run failed"; exit 1; }

gtkwave "$VHDL_FILE.vcd" || { echo "Gtkwave error"; exit 1; }


exit 0
