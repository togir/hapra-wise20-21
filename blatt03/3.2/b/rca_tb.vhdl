library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL; 

entity rca_tb is
end rca_tb;

architecture test of rca_tb is
  component rca
    port (
     a   : in std_logic_vector(7 downto 0);
     b   : in std_logic_vector(7 downto 0);
     sel : in std_logic;
     sum : out std_logic_vector(7 downto 0)
    );
  end component;

signal a, b, sum : std_logic_vector(7 downto 0);
signal sel : std_logic;
begin
  ripple_carry_adder: rca port map(a => a, b => b, sum => sum, sel => sel);

  process
    type pattern_type is record
        a, b, sum : std_logic_vector(7 downto 0);
    end record;
    type pattern_array is array (natural range <>) of pattern_type;

    constant test_array : pattern_array :=
    (
      ("00000000", "00000000", "00000000" ),
      ("00000000", "00000001", "00000001" ),
      ("00000001", "00000010", "00000011" ),
      ("00000100", "00000101", "00001001" ),
      ("00001010", "00001011", "00010101" ),
      ("00010110", "00010111", "00101101" ),
      ("00101110", "00101111", "01011101" )
    );

    constant substract_test_array : pattern_array :=
    (
      ("00000000", "00000000", "00000000" ),
      ("00000001", "00000000", "00000001" ),
      ("00000010", "00000001", "00000001" ),
      ("00000100", "00000011", "00000001" ),
      ("00001011", "00001010", "00000001" ),
      ("00010111", "00010110", "00000001" ),
      ("00101111", "00101110", "00000001" )
    );
  
  begin
    
    -- Test for addition

    for i in test_array'range loop
      
      --  Set the inputs.
      a   <= test_array(i).a;
      b   <= test_array(i).b;
      sel <= '1';

      --  Wait for the inputs to be set
      wait for 1 ns;

      --  Check the outputs.
      assert to_integer(unsigned(sum)) = to_integer(unsigned(test_array(i).sum))  
        report 
          integer'image(to_integer(unsigned(a))) & "," & 
          integer'image(to_integer(unsigned(b))) & ", " & 
          " reported sum: " & integer'image(to_integer(unsigned(sum))) & 
          " but should be " & integer'image(to_integer(unsigned(test_array(i).sum)));

    end loop;

    -- Test for substraction

    for i in substract_test_array'range loop
      
      --  Set the inputs.
      a   <= substract_test_array(i).a;
      b   <= substract_test_array(i).b;
      sel <= '0';
      --  Wait for the inputs to be set
      wait for 1 ns;

      --  Check the outputs.
      assert to_integer(unsigned(sum)) = to_integer(unsigned(substract_test_array(i).sum))  
        report 
          integer'image(to_integer(unsigned(a))) & "," & 
          integer'image(to_integer(unsigned(b))) & ", " & 
          " reported sum: " & integer'image(to_integer(unsigned(sum))) & 
          " but should be " & integer'image(to_integer(unsigned(substract_test_array(i).sum)));

    end loop;


    wait;
  end process;
end test;
