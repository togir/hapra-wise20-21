library ieee;
use ieee.std_logic_1164.all;

entity rca is
  port (
    a     : in  std_logic_vector(7 downto 0);
    b     : in  std_logic_vector(7 downto 0);
    sel   : in  std_logic; -- 1 = addition, 0 = substraction
    sum   : out std_logic_vector(7 downto 0)
  );
end rca;

architecture rtl of rca is

  pure function add_or_substract( val: std_logic; sel1: std_logic )
    return std_logic is
    begin
      if (sel1 = '1') then
        return val;
      end if;

      return not val;
    
  end function;

  component fa
    port (
      a : in std_logic;
      b : in std_logic;
      cin : in std_logic;
      cout : out std_logic;
      sum : out std_logic
    );
  end component;
  signal c_1: std_logic;
  signal c0 : std_logic;
  signal c1 : std_logic;
  signal c2 : std_logic;
  signal c3 : std_logic;
  signal c4 : std_logic;
  signal c5 : std_logic;
  signal c6 : std_logic;
  signal c7 : std_logic;
  signal b0 : std_logic;
  signal b1 : std_logic;
  signal b2 : std_logic;
  signal b3 : std_logic;
  signal b4 : std_logic;
  signal b5 : std_logic;
  signal b6 : std_logic;
  signal b7 : std_logic;

  
begin

  -- 
  c_1 <=add_or_substract('0', sel);
  b0 <= add_or_substract(b(0), sel);
  b1 <= add_or_substract(b(1), sel);
  b2 <= add_or_substract(b(2), sel);
  b3 <= add_or_substract(b(3), sel);
  b4 <= add_or_substract(b(4), sel);
  b5 <= add_or_substract(b(5), sel);
  b6 <= add_or_substract(b(6), sel);
  b7 <= add_or_substract(b(7), sel);

  add0 : fa port map(a => a(0), b => b0, cin => c_1, cout => c0, sum => sum(0));
  add1 : fa port map(a => a(1), b => b1, cin =>  c0, cout => c1, sum => sum(1));
  add2 : fa port map(a => a(2), b => b2, cin =>  c1, cout => c2, sum => sum(2));
  add3 : fa port map(a => a(3), b => b3, cin =>  c2, cout => c3, sum => sum(3));
  add4 : fa port map(a => a(4), b => b4, cin =>  c3, cout => c4, sum => sum(4));
  add5 : fa port map(a => a(5), b => b5, cin =>  c4, cout => c5, sum => sum(5));
  add6 : fa port map(a => a(6), b => b6, cin =>  c5, cout => c6, sum => sum(6));
  add7 : fa port map(a => a(7), b => b7, cin =>  c6, cout => c7, sum => sum(7));

end rtl;
