library ieee;
use ieee.std_logic_1164.all;

entity rca is
  port (
    a : in std_logic_vector(7 downto 0);
    b : in std_logic_vector(7 downto 0);
    --
    sum : out std_logic_vector(7 downto 0)
  );
end rca;

architecture rtl of rca is
  component fa
    port (
      a : in std_logic;
      b : in std_logic;
      cin : in std_logic;
      cout : out std_logic;
      sum : out std_logic
    );
  end component;
  signal c0 : std_logic;
  signal c1 : std_logic;
  signal c2 : std_logic;
  signal c3 : std_logic;
  signal c4 : std_logic;
  signal c5 : std_logic;
  signal c6 : std_logic;
  signal c7 : std_logic;
begin
  add0 : fa port map(a => a(0), b => b(0), cin => '0', cout => c0, sum => sum(0));
  add1 : fa port map(a => a(1), b => b(1), cin =>  c0, cout => c1, sum => sum(1));
  add2 : fa port map(a => a(2), b => b(2), cin =>  c1, cout => c2, sum => sum(2));
  add3 : fa port map(a => a(3), b => b(3), cin =>  c2, cout => c3, sum => sum(3));
  add4 : fa port map(a => a(4), b => b(4), cin =>  c3, cout => c4, sum => sum(4));
  add5 : fa port map(a => a(5), b => b(5), cin =>  c4, cout => c5, sum => sum(5));
  add6 : fa port map(a => a(6), b => b(6), cin =>  c5, cout => c6, sum => sum(6));
  add7 : fa port map(a => a(7), b => b(7), cin =>  c6, cout => c7, sum => sum(7));

-- ?
end rtl;
