library ieee;
use ieee.std_logic_1164.all;

entity ha_tb is
end ha_tb;

architecture test of ha_tb is
  component ha
      port (
        a: in std_logic;
        b: in std_logic;
        o: out std_logic;
        c: out std_logic
      );
  end component;

signal a, b, o, c : std_logic;
begin
  half_adder: ha port map(a => a, b => b, o => o, c => c);

  process
  
    type pattern_type is record
        --  The inputs of the gates.
        a, b, o, c : std_logic;
    end record;
    type pattern_array is array (natural range <>) of pattern_type;

    constant test_array : pattern_array :=
    (
      ('0', '0', '0', '0'),
      ('0', '1', '1', '0'),
      ('1', '0', '1', '0'),
      ('1', '1', '0', '1')
    );

   begin
--    a <= '0';
--    b <= '0';
--    wait 1 ns;
-- ADD MORE TEST CASES BELOW

    -- I've added the test cases as array so we can now loop over them

    for i in test_array'range loop
      
      --  Set the inputs.
      a <= test_array(i).a;
      b <= test_array(i).b;
      
      --  Wait for the inputs to be set
      wait for 1 ns;

      --  Check the outputs.
      assert o = test_array(i).o  
        report std_logic'image(a) & ", " & std_logic'image(b) & " reported outptut: " & std_logic'image(o) & " but should be " & std_logic'image(test_array(i).o);

    assert c = test_array(i).c
      report std_logic'image(a) & ", " & std_logic'image(b) & " reported carry: " & std_logic'image(c) & " but should be " & std_logic'image(test_array(i).c);

    end loop;

    wait;
  end process;
end test;
