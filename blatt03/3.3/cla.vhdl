library ieee;
use ieee.std_logic_1164.all;

entity cla is
  port (
    a : in std_logic_vector(3 downto 0);
    b : in std_logic_vector(3 downto 0);
    cin : in std_logic;
    cout : out std_logic;
    sum : out std_logic_vector(3 downto 0)
  );
end cla;

architecture rtl of cla is

  component clag
    port (
      gin : in std_logic_vector(3 downto 0);
      pin : in std_logic_vector(3 downto 0);
      cin : in std_logic;
      cout : out std_logic_vector(3 downto 0);
      pout : out std_logic;
      gout : out std_logic
    );
  end component;

  component facla
    port (
      a : in std_logic;
      b : in std_logic;
      cin : in std_logic;
      g : out std_logic;
      p : out std_logic;
      sum : out std_logic
    );
  end component;

  signal gin, pin, cout_1  : std_logic_vector(3 downto 0);
  signal pout, gout : std_logic;

  begin

    carry_look_ahead_genrator : clag port map (
      gin => gin,
      pin => gin,
      cin => cin,
      cout => cout_1,
      pout => pout,
      gout => gout
    );

    fa_0 : facla port map (
      a   => a(0),
      b   => b(0),
      cin => cin,
      g   => gin(0),
      p   => pin(0),
      sum => sum(0)
    );
  
    fa_1 : facla port map (
      a   => a(1),
      b   => b(1),
      cin => cout_1(0),
      g   => gin(1),
      p   => pin(1),
      sum => sum(1)
    );

    fa_2 : facla port map (
      a   => a(2),
      b   => b(2),
      cin => cout_1(1),
      g   => gin(2),
      p   => pin(2),
      sum => sum(2)
    );


    fa_3 : facla port map (
      a   => a(3),
      b   => b(3),
      cin => cout_1(2),
      g   => gin(3),
      p   => pin(3),
      sum => sum(3)
    );
end rtl;

