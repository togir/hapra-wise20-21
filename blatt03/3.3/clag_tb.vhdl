library ieee;
use ieee.std_logic_1164.all;

entity clag_tb is
end clag_tb;

architecture test of clag_tb is
  component clag
    port (
      gin : in std_logic_vector(3 downto 0);
      pin : in std_logic_vector(3 downto 0);
      cin : in std_logic;
      cout : out std_logic_vector(3 downto 0);
      pout : out std_logic;
      gout : out std_logic
    );
    end component;

  signal gin, pin, cout  : std_logic_vector(3 downto 0);
  signal cin, pout, gout : std_logic;

begin

  carry_look_ahead_genrator : clag port map (
    gin => gin,
    pin => gin,
    cin => cin,
    cout => cout,
    pout => pout,
    gout => gout
    );

  process

    type pattern_type is record
        --  The inputs of the gates.
        gin, pin, cout: std_logic_vector(3 downto 0);
    end record;
    type pattern_array is array (natural range <>) of pattern_type;

    constant test_array : pattern_array :=
    (
      ("0000", "0000", "0000"),
      ("0010", "0010", "0010"),
      ("0110", "0110", "0110"),
      ("1110", "1110", "1110"),
      ("0000", "1110", "0000"),
      ("0000", "0110", "0000"),
      ("0000", "0010", "0000"),
      ("0000", "1000", "0000")
    );

  begin
  

    for i in test_array'range loop
      gin   <= test_array(i).gin;
      pin   <= test_array(i).pin;
      cin <= '0';

      wait for 1 ns;

      assert cout = test_array(i).cout
        report "Test failed at index: " & integer'image(i);
      
    end loop;

    wait;

  end process;
  
end test;
