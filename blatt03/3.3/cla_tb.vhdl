library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL; 

entity cla_tb is
end cla_tb;

architecture test of cla_tb is
  component cla
    port (
      a : in std_logic_vector(3 downto 0);
      b : in std_logic_vector(3 downto 0);
      cin : in std_logic;
      cout : out std_logic;
      sum : out std_logic_vector(3 downto 0)
    );
  end component;

signal a, b, sum : std_logic_vector(3 downto 0);
signal cout, cin : std_logic;

begin
  adder: cla port map(a => a, b => b, cin => cin, cout => cout, sum => sum);

  process
    type pattern_type is record
        a, b, sum : std_logic_vector(3 downto 0);
    end record;
    type pattern_array is array (natural range <>) of pattern_type;

    constant test_array : pattern_array :=
    (
      ("0000", "0000", "0000" ),
      ("0000", "0001", "0001" ),
      ("0001", "0010", "0011" ),
      ("0100", "0101", "1001" ),
      ("0011", "0011", "0110" ),
      ("0110", "0101", "1011" ),
      ("1110", "1111", "1101" )
    );
  
  begin
    
    -- Test for addition

    for i in test_array'range loop
      
      --  Set the inputs.
      a   <= test_array(i).a;
      b   <= test_array(i).b;
      cin <= '0';

      --  Wait for the inputs to be set
      wait for 1 ns;

      --  Check the outputs.
      assert to_integer(unsigned(sum)) = to_integer(unsigned(test_array(i).sum))  
        report 
          integer'image(to_integer(unsigned(a))) & "," & 
          integer'image(to_integer(unsigned(b))) & ", " & 
          " reported sum: " & integer'image(to_integer(unsigned(sum))) & 
          " but should be " & integer'image(to_integer(unsigned(test_array(i).sum)));

    end loop;



    wait;
  end process;
end test;
