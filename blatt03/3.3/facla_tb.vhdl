library ieee;
use ieee.std_logic_1164.all;

entity facla_tb is
end facla_tb;

architecture test of facla_tb is
  component facla
   port (
    a : in std_logic;
    b : in std_logic;
    cin : in std_logic;
    g : out std_logic;
    p : out std_logic;
    sum : out std_logic
  );
  end component;

signal a, b, cin, g, p, sum : std_logic;

begin

  full_add_carry_look_ahead : facla port map (
    a   => a,
    b   => b,
    cin => cin,
    g   => g,
    p   => p,
    sum => sum
  );

  process 
    type pattern_type_1 is record
        --  The inputs of the gates.
        a, b, g, p : std_logic;
    end record;
    type pattern_array_1 is array (natural range <>) of pattern_type_1;

    constant test_array_1 : pattern_array_1 :=
    (
      ('0', '0', '0', '0' ),
      ('0', '1', '0', '1' ),
      ('1', '0', '0', '1' ),
      ('1', '1', '1', '1' )
    );

    type pattern_type_2 is record
        --  The inputs of the gates.
        a, b, cin, sum : std_logic;
    end record;
    type pattern_array_2 is array (natural range <>) of pattern_type_2;

    constant test_array_2 : pattern_array_2 :=
    (
      ('0', '0', '0', '0'),
      ('0', '0', '1', '1'),
      ('0', '1', '0', '1'),
      ('0', '1', '1', '0'),
      ('1', '0', '0', '1'),
      ('1', '0', '1', '0'),
      ('1', '1', '0', '0'),
      ('1', '1', '1', '1')
    );

  begin

    -- test G and P

    for i in test_array_1'range loop
    a   <= test_array_1(i).a;
    b   <= test_array_1(i).b;
    cin <= '0';

    wait for 1 ns;

    assert g = test_array_1(i).g
      report std_logic'image(a) & ", " & std_logic'image(b) & " reported G: " & std_logic'image(g) & " but should be " & std_logic'image(test_array_1(i).g);
      
    assert p = test_array_1(i).p
      report std_logic'image(a) & ", " & std_logic'image(b) & " reported P: " & std_logic'image(p) & " but should be " & std_logic'image(test_array_1(i).p);
    
    end loop;



    -- test sum

    for i in test_array_2'range loop
    a   <= test_array_2(i).a;
    b   <= test_array_2(i).b;
    cin <= test_array_2(i).cin;

    wait for 1 ns;

    assert sum = test_array_2(i).sum
      report std_logic'image(a) & ", " & std_logic'image(b) & " reported G: " & std_logic'image(g) & " but should be " & std_logic'image(test_array_2(i).sum);

    end loop;

    wait;
  end process;

end test;
