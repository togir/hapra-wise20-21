use work.ram_pack.ALL; 

entity ram_d_tb is
end ram_d_tb;

architecture structure of ram_d_tb is 
component ram
	generic (adr_breite:	Integer:=4);
	port (	adr:			in BIT_VECTOR(0 to adr_breite-1); 
			din:			in BYTE_4;
			do: 			out BYTE_4;
			read_write: 			in BIT);
end component;
Signal adresse:		BIT_VECTOR(0 to 3):= "0000";
signal din,do:		BYTE_4 := "00000000000000000000000000000000";
signal read_write:		BIT:='1';
begin

Testobject: ram
	port map(
		adr		=> adresse,
		din		=> din,
		do		=> do,
		read_write	=> read_write
	);

process 
begin
  adresse <= "0000";
  din <= "11111111111111111111111111111111";
  read_write <= '0';
  wait for 1 ns;
  read_write	<= '1';
  wait for 26 ns;
  read_write <= '0';
  wait for 20 ns;
  read_write <= '1';
  wait for 54 ns;

  adresse <= "1111";
  din <=	"00111111111111111111111111111100";
  read_write <= '0';
  wait for 1 ns;
  read_write	<= '1';
  wait for 26 ns;
  read_write <= '0';
  wait for 20 ns;
  read_write <= '1';
  wait for 54 ns;

  -- read the values we have written previously
  wait for 300 ns;
  adresse <= "0000";
  wait for 6 ns;
  read_write <= '1';
  wait for 44 ns;
  assert do = "11111111111111111111111111111111" report "Eingabe 1 ist nicht gepseichert";
  assert 2 = 1 report "Eingabe 1 wurde geprueft";
  wait for 56 ns;

  adresse <= "1111";
  wait for 6 ns;
  read_write <= '1';
  wait for 44 ns;
  assert do = "00111111111111111111111111111100" report "Eingabe 2 ist nicht gepseichert";
  assert 2 = 1 report "Eingabe 2 wurde geprueft";

 wait;

end process;

end structure;