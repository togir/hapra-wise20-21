---------------------------------------------------------------------------------------
----------------------------------Package Definition-----------------------------------
---------------------------------------------------------------------------------------
-- 1. Please create the package "ram_pack" by completing the following codes.

-- 2. Please check the "VHDL script" on Moodle to learn how to create a subtype, 
-- and then complete the subtype "BYTE_4", that consists of 4 BYTE (32 bits).

-- 3. Please check the "VHDL script" on Moodle to learn how to create a type, 
-- and then complete the type "SPEICHER_MATRIX", which is the storage units of RAM and 
-- belongs to "BYTE_4".

package ram_pack is
	subtype BYTE_4 is BIT_VECTOR (31 downto 0);
	type SPEICHER_MATRIX is array (integer range <>) of BYTE_4;
end ram_pack;

---------------------------------------------------------------------------------------
----------------------------------Entity Definition------------------------------------
---------------------------------------------------------------------------------------
-- 1. The way of recalling "ram_pack" is similar to the way of recalling other packages.

-- 2. "adr_breite" is an adjustable integer, that describes the length of the address,
-- "adr" is the address, "din" is the input, "do" is the output, "read_write" controls 
-- whether signals need to be read or written, the rising edge indicates "read" while the 
-- falling edge indicates "write".

use work.ram_pack.ALL; 
entity ram is
	generic (adr_breite : INTEGER);
	port (adr : in BIT_VECTOR (0 to adr_breite-1);
			din : in BYTE_4;
			do : out BYTE_4; 
			read_write : in BIT);
end ram;
architecture ram_behave of ram is

---------------------------------------------------------------------------------------
-----------------------------BitsToInteger Function------------------------------------
---------------------------------------------------------------------------------------
-- 1. You can directly use the function "to_integer", that can convert address fed 
-- as bits into an integer.

-- 2. With this converted integer, you can specify the position in SPEICHER_MATRIX 
-- where the data are read or written.


function to_integer (constant i : in BIT_VECTOR) return INTEGER is
	-- Funktion: Umwandeln eines Bit-Vektor-Wertes in eine Integer-Zahl
	variable i_tmp : BIT_VECTOR (0 to i'length-1);
	variable int_tmp : INTEGER := 0;	
begin
	i_tmp := i;
	for m in 0 to i'length-1 loop
		if i_tmp(m) = '1' then
			int_tmp := int_tmp + 2**(i'length-1 - m); 
		end if;
	end loop;
	return int_tmp;
end to_integer;

-----------------------------------------------------------------------------------------
-----------------------------Lesezugriff Process-----------------------------------------
-----------------------------------------------------------------------------------------
-- 1. The signal "ram_m" is an instance of SPEICHER_MATRIX.

-- 2. In the "Lesezugriff process", data are extracted from RAM (SPEICHER_MATRIX).

-- 3. In the "Lesezugriff process", we need to check whether the signals satisfy the 
-- requirements (time constraints) in the assignments.

-- 4. If the above requirements (time constraints) are not satisfied, "Fehler beim Lesen"
-- is reported.

-- 5. The following codes demonstrates how the "Lesezugriff process" works, please read them
-- carefully and try to complete the rest of the processes.

	signal ram_m : SPEICHER_MATRIX (0 to (2**adr_breite)-1);
begin
	Lesezugriff: process 
	variable t_read, t_akt : TIME; ---- two varaibles to check the timmings
	begin
		wait on adr;  ---- we wait till the adress changes
		wait for 6 ns;   ---- as stated in abbildung 2: the adress changes 6 ns before the read/write signal gets changed. This is done to give the hardware enough time to "propagate" the adress changes.
		if read_write = '1' then  ---- we only want to read data if there is a rising_edge.
			do <= ram_m(to_integer(adr)); -- Because we implemented the ram as an array we need to tranform our binary number into a decimal number (vhdl uses decimal numbers for array indices). The value at the specified index is written to our do (Digital Output) port.
		end if;
		
		wait for 44 ns; ---- To gurantee our "hardware" has enough time to read the date the adress must be availabe 50ns. We waited 6ns before starting to read and now the additional 44ns. 
		t_read := read_write'last_event;  ---- how much time has passed since the las change of read_write
		if t_read < 44 ns then -- after 50ns we check if the read_write adress was not changed for 50ns if not we will print an error
			report "Fehler beim Lesen"
			severity WARNING;
		end if;
	end process;

-----------------------------------------------------------------------------------------
-----------------------------Schreibzugriff Process--------------------------------------
-----------------------------------------------------------------------------------------
-- 1. According to the requirements in the assignment, please complete the "Schreibzugriff" 
-- process where the data are fed into RAM (SPEICHER_MATRIX). 

-- 2. In the "Schreibzugriff process", different time constraints should be satisfied. 
-- E.g., the data input should be shorter than 25ns after the address signal changed, and
-- longer than 20ns before the read_write signal changes from "high" to "low".

-- 3. If the data input is shorter than 20ns after the address changed, please report 
-- "Daten liegen noch nicht lang genug an".

-- 4. If the data input is longer than 25ns before the read_write changes from "high" to "low",
-- please report "Daten lagen nicht schnell genug an".

	Schreibzugriff: process
  begin
    wait on adr;
	  wait on read_write; ---- wait for signal change on read_write
    
    if (read_write = '0') then ---- check if signal change was a falling flank
        assert din'last_event > 20 ns report "Daten liegen noch nicht lang genug an" severity WARNING;

      assert adr'last_event - din'last_event < 25 ns report "Daten lagen nicht schnell genug an" severity WARNING;
        
      ram_m(to_integer(adr)) <= din;
    end if;
	end process;



-----------------------------------------------------------------------------------------
------------------------Zyklus_Pruefen und Schreibimpulse_Pruefen------------------------
-----------------------------------------------------------------------------------------
-- 1. According to the requirements in the assignment, please complete the "Zyklus_Pruefen" 
-- process and "Schreibimpulse_Pruefen". 

-- 2. In both processes, the time constraints need to be checked.

-- 3. If the address is not held for 100 after the address changed, please report 
-- "Zykluszeit wurde nicht eingehalten".

-- 4. If the Schreibimpulse (the period where the read_write signal equals "0") is shorter
-- than 15 ns, please report "Schreib-Impuls zu kurz".

-- 5. If the data input is not stable during Schreibimpulse (data changed), please report 
-- "Daten waren beim Schreiben nicht stabil!".		

	Zyklus_Pruefen: process (adr)
    variable first: Boolean := true;
    variable last_change, current_change : TIME;
	begin
   if (first) then
    first := FALSE; 
    current_change := now;
    else 
      last_change := current_change;
      current_change := now;
      assert (current_change - last_change) > 100 ns report "Zykluszeit wurde nicht eingehalten" severity WARNING;
    end if;
	end process;


	Schreibimpulse_Pruefen: process
	  variable t_rw_changed: Time;
    variable last_change, current_change : TIME;
	begin
	 wait on read_write;
    last_change := now;
   	wait on read_write;
    current_change := now;

   t_rw_changed := read_write'last_event;
      assert (current_change - last_change) > 15 ns report "Schreib-Impuls zu kurz" severity WARNING;
      assert din'last_event > 16 ns report "Daten waren beim Schreiben nicht stabil!" severity WARNING; -- why 16 and not 15 ???
	end process;
	
end ram_behave;

