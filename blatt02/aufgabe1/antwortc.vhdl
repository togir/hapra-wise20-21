library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;

entity antwortc is
end antwortc;

architecture test of antwortc is
  signal a, b, c : std_logic;
begin
  process

   type pattern_type is record
      --  The inputs of the adder.
      i0, i1 : std_logic;
    end record;
    type pattern_array is array (natural range <>) of pattern_type;

    constant patterns : pattern_array :=
    (('0', '0'),
    ('0', '1'),
    ('1', '0'),
    ('1', '1'),
    ('0', 'Z'),
    ('1', 'Z'),
    ('0', 'W'),
    ('0', 'W'));
  begin
    --  Check each pattern.
    for i in patterns'range loop
      --  Set the inputs.
      a <= patterns(i).i0;
      b <= patterns(i).i1;
      --  Wait for the results.
      wait for 1 ns;
      --  Check the outputs.
    report std_logic'image(a) & " and " & std_logic'image(b) & " -> "  & std_logic'image(a and b);
    report std_logic'image(a) & " or " & std_logic'image(b) & " -> "  & std_logic'image(a or b);
    end loop;
    wait;
  end process;
end test;
