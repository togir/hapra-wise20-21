1) Fuer einen Punkt ist das echt zu viel aufwand!


2)
Weitere Zustände:
- U Uninitailzed
- '-' Don't care

Der Zustand Uninitailzed wird benutzt wenn dem "signal" im vhdl-code noch kein wert zugewiesen wurde. Das anliegende Signal ist also nicht bekannt.

Der Zustand Don't care wird benutzt wenn ein "signal" keinen einfluss auf eine schaltung haben soll. Der Wert don't care kann bei der ausfuehrung auf 0 oder 1 gewählt werden.

3)
'0' and '0' -> '0'
'0' or '0' -> '0'
'0' and '1' -> '0'
'0' or '1' -> '1'
'1' and '0' -> '0'
'1' or '0' -> '1'
'1' and '1' -> '1'
'1' or '1' -> '1'
'0' and 'Z' -> '0'
'0' or 'Z' -> 'X'
'1' and 'Z' -> 'X'
'1' or 'Z' -> '1'
'0' and 'W' -> '0'
'0' or 'W' -> 'X'
'0' and 'W' -> '0'
'0' or 'W' -> 'X'

