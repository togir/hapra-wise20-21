library ieee;
use ieee.std_logic_1164.all;

entity signals_tb is
end signals_tb;

architecture test of signals_tb is
  signal a, b, c : std_logic;
begin
  process begin
    a <= '0';
    b <= '0';
    -- a= 0, b=0, c=U
    wait for 10 ns;
    a <= '0';
    b <= '1';
    -- a=0, b=1, c=U
    wait for 10 ns;
    a <= '1';
    b <= '0';
    -- a=1, b=0, c=U
    wait for 10 ns;
    a <= '1';
    b <= '1';
    -- a=1, b=1, c=U
    wait for 10 ns;
    a <= 'X';
    b <= 'W';
    -- a=kein wert rot, b=w, c=U
    wait for 10 ns;
    assert false report "End of test";
    wait;
  end process;
end test;
