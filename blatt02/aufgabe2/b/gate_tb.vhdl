library ieee;
use ieee.std_logic_1164.all;

-- create testbench for and_gate
-- can be left empty, since test bench (this file) has no inputs or outputs
entity gate_tb is
end gate_tb;

architecture test of gate_tb is
  -- component declaration, so that it can be instantiated later
  -- must have same name and port list as the entity
  component nandgate
      port (
        input1 : in std_logic;
        input2 : in std_logic;
        result : out std_logic
      );
  end component;
  component andgate
      port (
        input1 : in std_logic;
        input2 : in std_logic;
        and_result : out std_logic
      );
  end component;
  component orgate
      port (
        input1 : in std_logic;
        input2 : in std_logic;
        result : out std_logic
      );
  end component;
  component norgate
      port (
        input1 : in std_logic;
        input2 : in std_logic;
        result : out std_logic
      );
  end component;
  component xorgate
      port (
        input1 : in std_logic;
        input2 : in std_logic;
        result : out std_logic
      );
  end component;
  component xnorgate
      port (
        input1 : in std_logic;
        input2 : in std_logic;
        result : out std_logic
      );
  end component;
  component notgate
      port (
        input1 : in std_logic;
        result : out std_logic
      );
  end component;
  

-- define the signals that will be used in test
  signal a, b, and_o, nand_o, or_o, nor_o, xor_o, xnor_o, not_o : std_logic;
-- begin test code
begin

  and_g:  andgate  port map(input1 => a, input2 => b, and_result => and_o);
  nand_g: nandgate port map(input1 => a, input2 => b, result => nand_o);
  or_g:   orgate   port map(input1 => a, input2 => b, result => or_o);
  nor_g:  norgate  port map(input1 => a, input2 => b, result => nor_o);
  xor_g:  xorgate  port map(input1 => a, input2 => b, result => xor_o);
  xnor_g: xnorgate port map(input1 => a, input2 => b, result => xnor_o);
  not_g:  notgate  port map(input1 => a,              result => not_o);

  process 

  type pattern_type is record
      --  The inputs of the gates.
      i0, i1, res : std_logic;
    end record;
    type pattern_array is array (natural range <>) of pattern_type;

    constant and_array : pattern_array :=
    (('0', '0', '0'),
    ('0', '1', '0'),
    ('1', '0', '0'),
    ('1', '1', '1'));

    constant or_array : pattern_array :=
    (('0', '0', '0'),
    ('0', '1', '1'),
    ('1', '0', '1'),
    ('1', '1', '1'));

    constant xor_array : pattern_array :=
    (('0', '0', '0'),
    ('0', '1', '1'),
    ('1', '0', '1'),
    ('1', '1', '0'));

  begin
    -- test and, nand, or, nor, xor, xnor
    for i in and_array'range loop
      --  Set the inputs.
      a <= and_array(i).i0;
      b <= and_array(i).i1;
      --  Wait for the inputs to be set
      wait for 1 ns;
      --  Check the outputs.

    -- check and & nand
    assert and_o = and_array(i).res  report std_logic'image(a) & " and " & std_logic'image(b) & " reported " & std_logic'image(and_o) & " but should be " & std_logic'image(and_array(i).res);

    assert nand_o = not(and_array(i).res) report std_logic'image(a) & " nand " & std_logic'image(b) & " reported " & std_logic'image(nand_o) & " but should be " & std_logic'image(not(and_array(i).res));
 
    
    -- check or and nor
    assert or_o = or_array(i).res report std_logic'image(a) & " or " & std_logic'image(b) & " reported " & std_logic'image(or_o) & " but should be " & std_logic'image(or_array(i).res);
    assert nor_o = not(or_array(i).res) report std_logic'image(a) & " nor " & std_logic'image(b) & " reported " & std_logic'image(nor_o) & " but should be " & std_logic'image(not(or_array(i).res));
    

    -- check xor and xnor
    assert xor_o = xor_array(i).res report std_logic'image(a) & " xor " & std_logic'image(b) & " reported " & std_logic'image(xor_o) & " but should be " & std_logic'image(xor_array(i).res);

    assert xnor_o = not(xor_array(i).res) report std_logic'image(a) & " xnor " & std_logic'image(b) & " reported " & std_logic'image(xnor_o) & " but should be " & std_logic'image(not(xor_array(i).res));

    end loop;

    a <= '0';
    wait for 1 ns;
    assert not_o = '1' report "not 0 reports" & std_logic'image(not_o) & " but should be 1";

    a <= '1';
    wait for 1 ns;
    assert not_o = '0' report "not 1 reports" & std_logic'image(not_o) & " but should be 0";

    report "All test passed!";
    wait;
  end process;
end test;
